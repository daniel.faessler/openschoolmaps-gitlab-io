Title: Unterricht und Knowhow mit offenen Karten
Date: 2018-07-11
Category: OpenSchoolMaps
Tags: meta, mission statement
Status: hidden
Url:
Save_as: index.html

![schoolkids]({static}/images/schoolkids.jpg)

OpenSchoolMaps ist ein kleines Projekt des [Geometa Lab OST](https://www.ost.ch/ifs) zur Förderung von offenen Karten und Geo-Informations-Systemen (GIS). 
Dabei werden vor allem offene Daten von OpenStreetMap, Open Source Software und freie Dienste verwendet. 

Das Projekt richtet sich an Selbstlerner sowie an Lehrerinnen und Lehrer der Sekundarstufe II (Sekundarschulen und 
Gymnasien) in den Fächern "Medien und Informatik" und "Natur, Mensch, Gesellschaft" (u.a. Geographie) sowie an Studierende.

Sowohl die Karten und Geodaten als auch die Lehr- und Lernmaterialien (Tutorials) werden 
als Open Educational Resource ([OER](https://de.wikipedia.org/wiki/Open_Educational_Resources)) 
bereitgestellt, so dass jede/r die Lehr- und Lernmaterialien frei nutzen kann - 
auch ausserhalb des Unterrichts - und auch so dass jede/r selber dazu beitragen kann!
Die Unterlagen sind auf dem [OpenSchoolMaps-Repository](https://gitlab.com/openschoolmaps/openschoolmaps.gitlab.io/) verfügbar.

## Karten, Karten

Die Vision dieses Projekts ist, dass jedermann/-frau selber eine Karte erstellen kann sowie sich Kompetenzen aneignen kann im Umgang mit dem Management mit GIS und der Analyse von Geodaten. 
Darum gibt es hier nicht Karten zu sehen, sondern Einführungen in die Erstellung verschiedener Karten. 
Wer Karten sucht, der kann sich beispielsweise bei den 1001 Points-of-Interest der [OpenPOIMap](http://openpoimap.org/?map=amenity&zoom=16&lat=47.3632&lon=8.53992&layers=B00FTFFFFFFFFFFFFFFFFFFFFFFFFFFFF) 
oder bei [Qwant Maps](https://www.qwant.com/maps/place/latlon:47.36720:8.54440#map=15.31/47.3665181/8.5446066) inspirieren lassen - abgesehen natürlich von der Standardkarte 
auf [OSM.ch](https://www.osm.ch) und [OpenStreetMap.org](https://www.openstreetmap.org). 
Auch gibt es u.a. eine Einführung in GIS mit Hilfe der Open Source-Software QGIS. 

## News

- Es geht Schlag auf Schlag und schon gibt es ein weiteres Arbeitsblatt auf OpenSchoolMaps: ["Zeitreise mit QGIS!"](https://openschoolmaps.ch/pages/materialien.html#zeitreise-mit-qgis) Der Titel bezieht sich metaphorisch auf die Möglichkeiten von QGIS, zeitbezogene Geodaten zu visualisieren und animierte GIFs zu erstellen. Als Beispiel der freien Anleitung dient die Entwicklung des Gebäudebestandes der Stadt Zürich von 1200 bis 2020. (27. Januar 2024)
- Gerade rechtzeitig vor Semesterbeginn gibt es ein weiteres Arbeitsblatt ["Der perfekte Studi-Wohnort! Eine Standortanalyse mit QGIS"](https://openschoolmaps.ch/pages/materialien.html#der-perfekte-studi-wohnort). Darin wird gezeigt, wie man mit QGIS Daten des Bundesamtes für Raumentwicklung (ARE) und von OpenStreetMap (OSM) nutzt, um potenzielle Wohngebiete für wohnungssuchende Studierende zu finden. (14. Januar 2024)
- Nach fünf Monaten Arbeit haben wir wieder zwei neue Arbeitsblätter im Abschnitt "Weitere Arbeitsblätter zu QGIS 3 und Geoinformationssystemen (GIS)" publiziert: Arbeitsblatt "[Erstellen von Ausschnitten aus Rasterdaten mit QGIS](https://openschoolmaps.ch/pages/materialien.html#rasterdaten-ausschneiden)" und Arbeitsblatt "[Erstellen von Ausschnitten aus Vektordaten mit QGIS](https://openschoolmaps.ch/pages/materialien.html#vektordaten-ausschneiden)". (11. Dezember 2023)
- Neues Arbeitsblatt "[Geokodieren von Adressen](https://openschoolmaps.ch/pages/materialien.html#geokodieren-von-adressen)" publiziert im Abschnitt "Weitere Arbeitsblätter zu QGIS 3 und Geoinformationssystemen (GIS)". (15. Juni 2023)
- Auf OpenSchoolMaps tut sich etwas: Ein Arbeitsblatt zum Thema "Erstellen von Ausschnitten aus Raster- und Vektordaten mit QGIS" ist in Vorbereitung. Die Fertigstellung wird leider noch einige Zeit in Anspruch nehmen. In der Zwischenzeit kann das Online-Tutorial zum Thema [Rasterdaten ausschneiden mit QGIS (hier)](https://md.coredump.ch/s/eiLVdmDGk#) und zum Thema [Vektordaten ausschneiden mit QGIS (hier)](https://md.coredump.ch/s/G7L_gxMBh#) aufgerufen werden. (29. Januar 2023)
- Neu kann auf OpenSchoolMaps genau die Stelle angezeigt werden, wo sich ein Material bzw. Arbeitsblatt befindet. Hier z.B. der sog. Anker-Link zu "uMap der Karteneditor": [https://openschoolmaps.ch/pages/materialien.html#umap-der-karteneditor](https://openschoolmaps.ch/pages/materialien.html#umap-der-karteneditor). Eine solche URL erhält man, wenn man im Browser eine Überschrift anwählt und sich mit "Inspect" oder "Quelltext" den Anker-Text anzeigen lässt. (14. Juni 2022)
- Neues Arbeitsblatt [Tabellen verknüpfen ("joinen")](https://openschoolmaps.ch/pages/materialien.html#tabellen-verknuepfen) publiziert im Abschnitt "Weitere Arbeitsblätter zu QGIS 3 und Geoinformationssystemen (GIS)". (4. Mai 2022)
- New worksheet published with the title "Joining Tables". It's a worksheet about understanding the relationships among tables by using QGIS. It is in English. That's why you can only find it in the zip file at the bottom of the materials page. Soon the German version will be ready too. (5. April 2022)
- Neues Arbeitsblatt mit dem Titel "Daten sichten, bereinigen und integrieren mit OpenRefine" publiziert im Abschnitt "Weitere Informatik-Themen und -Werkzeuge" unter "Materialien". (1. Oktober 2021)
- Neues Arbeitsblatt "Betriebssystem-Shells und Command-Line Interfaces" publiziert unter dem neuen Abschnitt "Weitere Informatik-Themen und -Werkzeuge:" im Tab "Unterrichtsmaterialien". Schlüsselwörter: GUI, CLI, Terminal, Betriebssystem-Shell (Shell), Prompt, Systembefehl, Verzeichnis, Pfad, Umgebungsvariable, ogr, ili2gpkg, Geodaten, Konvertieren. (2. Juli 2021)
- Neues Arbeitsblatt "Aufbereiten und Visualisieren von Fotos mit Koordinaten" publiziert. Schritt für Schritt Fotos - beispielsweise von Mobile Phones -, die mit Geotags (GPS) versehen sind, in QGIS importieren und auf der Karte anzeigen. (23. Januar 2021)
- Zwei Cheatsheets - eines zu QGIS und eines zu OpenStreetMap Tagging - veröffentlicht. (Dezember 2020)
- Neues Arbeitsblatt publiziert zu "Tabellen im Web mit Koordinaten in CSV-Dateien konvertieren". Dieses zeigt wie man im Web (inklusive Wikipedia) verfügbare Tabellen mit Koordinaten in eine (Geo-)CSV-Datei umwandelt und in QGIS lädt ohne zu programmieren. (16. Dezember 2020)
- Neues Arbeitsblatt zu "Raumanalyse mit Hektarrasterdaten" publiziert mit Hektarrasterdaten vom BfS (Bevölkerungsstatistik) und von OpenStreetMap (Schulen). Zudem die 5 Arbeitsblätter zur Einführung in QGIS aktualisiert aufgrund von vielseitigem Feedback. (6. November 2020)
- Ein neues Unterrichtsmaterial publiziert: ein Klimapostenlauf! Es ist dies ein Orientierungslauf mit dem Handy zum Thema Klima! (25. September 2020)
- Sämtliche 35(!) Arbeitsblätter von OpenSchoolMaps sind nun nicht nur als herunterladbare PDF sondern auch als Webseiten (HTML) verfügbar. Dies verbessert nochmals die Auffindbarkeit und Nutzbarkeit der Lehr- und Lern-Materialien von OpenSchoolMaps. (28. August 2020)
- Am **3. Juni 2020** organisiert das GEOSchoolDay-Team ab 16 Uhr ein **Webinar zu "Geographie im Fernunterricht – Erfahrungen der Sekundarstufe I und II"**. Dabei sollen Erfahrungen im Fernunterricht zu geographischen Themen ausgetauscht werden. Anmeldung und [weitere Informationen...](https://geoschoolday.ch)
- Neues Arbeitsblatt "Einführung in den OpenStreetMap-Editor JOSM" hinzugefügt, das von Christian Nüssli erarbeitet wurde. Vielen Dank Christian! (30. April 2020)
- Am 25. April 2020 gab es einen Online-Workshop zu "[OpenSchoolMaps - Materialien zu OpenStreetMap](https://openeducationday.ch/programm-2020/openschoolmaps/)" im Rahmen des Virtuellen Open Education Halfday 2020.
- Two new worksheets on "Apache Superset the Data Visualization and Publication Tool" published. (25. November 2019)
- Zwei neue Arbeitsblätter zum Thema "Apache Superset das Datenvisualisierungs- und -Publikations-Werkzeug" publiziert. (25. Oktober 2019)
- OpenSchoolMaps gewinnt den DINAcon Award 2019 in der Kategorie **"Best Open Education Project"**! Mit den DINAcon Awards werden Open-Projekte von Communities, Unternehmen, Verwaltungen, Organisationen und Einzelpersonen ausgezeichnet. [Weblink](https://awards.dinacon.ch/awards/openschoolmaps/). (18. Oktober 2019)
- Am 18. August 2019 wurde eine [Wiki-Seite 'Einführung in JOSM' angelegt](https://gitlab.com/openschoolmaps/openschoolmaps.gitlab.io/wikis/Einf%C3%BChrung-in-JOSM) mit kommentierten Links über diesen Editor.
- Le 16 août 2019, les feuilles de travail pour "OSM.org comme visualiseur de carte" et "Editer et utiliser OpenStreetMap" ont été mises à disposition en français (voir archive Zip ci-dessous sur le matériel didactique).
- On June 27, 2019, OpenSchoolMaps was supplemented with special teaching material entitled "Mapping the surroundings yourself". An information for instructors and a worksheet for participants explain how to organize an "outdoor mapping event" (including mapping parties and project days for school classes).
- Am 21. Juni 2019 wurden zwei neue Anleitungen mit dem aktuellen Thema "Die Umgebung selber kartografieren - Outdoor Mapping Event" publiziert und zwar einerseits eine Anleitung für Lehrpersonen und Instruktoren und andererseits eine Anleitung für Teilnehmende, d.h. für Schülerinnen und Schüler und für Interessierte.
- Am 21. Juni 2019 wurde die "Kontakt"-Webseite erweitert um "Kontakt & Rechtliches".
- Ende Mai 2019 ein weiteres Arbeitsblatt "OpenStreetMap-Daten beziehen und mit QGIS 3 nutzen" hinzugefügt.
- On May 8, 2019 further worksheets for QGIS 3 and Geoinformation Systems (GIS) added, namely "Spatial analysis with vector data - Motorway" and "Spatial analysis with raster data - Where the chamois live".
- Am 2. Mai 2019 weitere Arbeitsblätter zu QGIS 3 und Geoinformationssystemen (GIS) hinzugefügt und zwar "Raumanalyse Vektordaten - Autobahn" sowie "Raumanalyse Rasterdaten - Wo die Gämsen wohnen".
- Workshop zu "OER in der Schule - OpenSchoolMaps und H5P" von Nicole Berva und Stefan Keller am Sa. 6. April 2019: "Open Education Day 2019" in Bern. <https://openeducationday.ch/>
- Vortrag von Stefan Keller zu "OpenSchoolMaps - Unterricht mit offenen Karten" am Sa. 16. März in Wil/SG an der ICT-Kadervernetzung 2019 vor Kursleitenden in Medien und Informatik aller Ostschweizer Kantone sowie Liechtenstein. <http://educanet2.ch/kadervernetzung/.ws_gen/>
- All worksheets (PDF) from OpenSchoolMaps - including the introductions to OpenStreetMap and to QGIS 3 - are available in English now. (2019-03-15) 
- New post on Geometa Lab HSR Blog published 2018-01-15, about ["Creating a Thematic Online-Map using uMap"](http://geometalab.tumblr.com/post/182036823612/creating-a-thematic-online-map-using-umap).
- Am Sa. 6. Oktober 2018 hielt Stefan Keller anlässlich der WikiCon 2018 in St. Gallen einen Vortrag mit dem Titel "OpenSchoolMaps mit OpenStreetMap und Geodaten editieren mit QGIS". <https://wikicon.org>
- Am 3. Juli 2018 wurde anlässlich der Opendata.ch/2018-Konferenz in St. Gallen ein "Community Showcase" vorgestellt mit dem Titel "OpenSchoolMaps - OpenStreetMap als Open Educational Resource". <https://hack.opendata.ch/project/204>
- Am 6. und 7. Juni 2018 gab es einen Edu Workshop für Lehrpersonen am GEOSchool Day in Bern. Der Beitrag von Stefan Keller hatte den Titel "OpenStreetMap". <http://geoschoolday.ch/> (vgl. Folien).
- Am 28. April 2018 wurde anlässlich des "Open Education Day 2018", FHNW Brugg, erstmals von Stefan Keller OpenSchoolMaps vorgestellt Hier <https://openeducationday.ch/das-programm/die-freie-weltkarte-openstreetmap-grenzueberschreitend-nutzen/> die Folien und hier der Workshop "Die freie Weltkarte OpenStreetMap grenzüberschreitend nutzen" <https://openeducationday.ch/>

Bildquelle: Thomas Ingold 2018
