= Zeitreisen mit QGIS: Temporale Daten visualisieren und animierte GIFs erstellen
OpenSchoolMaps.ch -- Freie Lernmaterialien zu freien Geodaten und Karten

:xrefstyle: short
:imagesdir: ../../../bilder/
:experimental:
include::../../../snippets/lang/de.adoc[]

Ein Arbeitsblatt von Claudio Bertozzi, Daniel Fässler und Stefan Keller. 
ifdef::show_solutions[- *LÖSUNGEN*]

== Einleitung

Dieses Arbeitsblatt erklärt, wie temporale Daten wirkungsvoll mit Hilfe von QGIS visualisiert werden können. Der Begriff "`temporale Daten`" bezieht sich auf Raum-Informationen, die eine zeitliche Komponente enthalten. Räumlich-zeitliche Daten sind Informationen, die sowohl räumliche (Ort) als auch zeitliche (Zeit) Aspekte enthalten. Dadurch können Entwicklungen und Veränderungen an einem bestimmten Ort über einen bestimmten Zeitraum dargestellt werden. Konkret wird dies am Beispiel der städtischen Bebauung in Zürich dargestellt, wobei die historische Entwicklung der Bautätigkeit im Zeitverlauf betrachtet wird.


.Entwicklung des Gebäudebestandes der Stadt Zürich als animiertes GIF, erstellt mit QGIS und mit Hilfe dieses Arbeitsblattes. (*Hinweis:* Das animierte GIF ist vor allem in Browsern sichtbar; gängige PDF-Viewer unterstützen eine solche Animation nicht und zeigen nur das erste Bild).
[#gifmap]
image::zeitreise_mit_gqis_temporale_daten_visualisieren_gifs_erstellen/map.gif[]


Die “Zeitreise” im Titel bezieht sich metaphorisch auf die Möglichkeiten von QGIS, zeitbezogene Geodaten zu visualisieren. Es geht also nicht um tatsächliche Zeitreisen, sondern darum, wie man mit GIS-Technologie historische Daten im Zeitverlauf darstellen kann, z.B. durch animierte Karten oder Grafiken.

In den folgenden Kapiteln wird die Theorie zum Umgang mit zeitbezogene Geodaten erläutert und durch praktische Übungen veranschaulicht, um einen Einblick in die Arbeit in QGIS zu geben. Der Schwerpunkt liegt dabei auf zwei Punkten:

. Wie können temporale Daten dargestellt werden?
. Wie können diese mit zusätzlichen Informationen ergänzt werden?


=== Ziele

*Zielkompetenzen*: Die Ziele dieses Arbeitsblattes sind:

* Ein Verständnis für temporale Daten mit Ortsbezug aufbauen
* Darstellen temporaler Daten
* Die Darstellung mit zusätzlichen Informationen ergänzen
* Exportieren solcher Daten als animiertes GIF

*Zielgruppe*: Angesprochen sind Selbstlerner, Studierende, Schülerinnen, Schüler und Lehrpersonen auf Sekundarstufe II (Sekundarschulen und Gymnasien).


=== Zeitplanung

Zeitaufwand: Diese Übung dauert ca. 60 Minuten.


=== Voraussetzungen

Im folgenden gibt es eine Liste der benötigten Software und Hardware sowie Angaben zu den vorausgesetzten Kompetenzen.

[horizontal]
Software-/Hardware-Voraussetzungen:: Als Software wird https://download.qgis.org/[QGIS 3] vorausgesetzt. Dieses Unterrichtsmaterial wurde mit der neusten herunterladbaren QGIS Version und mit dem neusten QGIS Long Term Release (LTR) getestet. QGIS läuft auf den gängigen Betriebssystemen Windows, MacOS und Linux.

Vorbereitungen:: QGIS installiert.

Eingangskompetenzen:: Es wird vorausgesetzt, dass Grundkenntnisse im Umgang mit einem Betriebssystem sowie Grundkenntnisse in QGIS 3 vorhanden sind.


=== Vorbereitungen

Nach der Installation der benötigten Software muss als Daten-Grundlage muss eine Zip-Datei heruntergeladen werden. Darin ist folgende GeoPackage-Datei "`Gebaeude Zuerich.gpkg`" enthalten: *Zip Datei in OpenSchoolMaps*, die dann in QGIS geöffnet werden kanne wie der nachfolgende Screenshot zeigt.

image::zeitreise_mit_gqis_temporale_daten_visualisieren_gifs_erstellen/img1.png[]



== Theorie: Grundlagen der temporalen Datenhaltung

Das Konzept der temporalen Datenhaltung kommt aus der Informationstechnik, das die zeitliche Entwicklung von Daten bei Speicherung in einer Datenbank festhält. Es ermöglicht die Rekonstruktion von Datenwerten zu einem bestimmten Zeitpunkt in der Vergangenheit oder Zukunft. Es gibt zwei Arten der zeitlichen Betrachtung: Gültigkeitszeit und Transaktionszeit. Die Gültigkeitszeit ist der Zeitraum, in dem ein Datenelement in der realen Welt gültig ist, während die Transaktionszeit der Zeitpunkt ist, an dem ein Datenelement im Datenbestand gespeichert wurde. Temporale Datenhaltung ist erforderlich, wenn alle Änderungen an den Daten dokumentiert werden müssen. 

In unserem spezifischen Anwendungsfall ist die Gültigkeitszeit von besonderer Relevanz. In Kombination mit räumlichen Geodaten ermöglicht die temporale Datenhaltung eine detaillierte Nachverfolgung von räumlichen Veränderungen über die Zeit. Dies kann zur Veranschaulichung verschiedenster Phänomene dienen, wie etwa der Entwicklung von Umweltmerkmalen, urbaner Expansion oder Veränderungen in der Landnutzung.

TIP: *Anmerkung*: Zeitzonen und Sommer-/Winterzeit sind ebenfalls ein häufiges Problem bei Zeitangaben. Dies ist jedoch nicht Gegenstand dieses Arbeitsblattes.

Wir wollen die Veränderung des Gebäudebestandes in der Stadt Zürich darstellen. Dazu wird ein Datensatz für Gebäude und Wohnungen im Kanton Zürich verwendet. Der Datensatz wurde von opendata.swiss bezogen, für die Stadt Zürich gefiltert und mit dem Datumsfeld "`baujahr`" ergänzt. Im bestehenden Datensatz liegt das Baujahr nur als numerischer Wert vor, für die Zeitsteuerung wird jedoch ein Feld vom Typ Datum benötigt.

Es wurden alle Werte ausserhalb einer gewissen Zeitspanne (engl. range) und solche ohne Baujahr herausgefiltert.

image::zeitreise_mit_gqis_temporale_daten_visualisieren_gifs_erstellen/img2.png[]

Zudem wurden die Punkte bereits gruppiert dargestellt, sodass die ältesten Gebäude möglichst dunkel und die neusten möglichst hell dargestellt werden.

image::zeitreise_mit_gqis_temporale_daten_visualisieren_gifs_erstellen/img3.png[]

== Theorie: Zeitsteuerung

In den Layereigenschaften gibt es einen Menüpunkt "`Zeitlich`" (1). Da kann für einen Layer die dynamische Zeitsteuerung aktiviert werden (2).

image::zeitreise_mit_gqis_temporale_daten_visualisieren_gifs_erstellen/img4.png[]

Ist diese aktiviert, kann zwischen verschiedenen Konfigurationen ausgesucht werden. Mehr Details dazu sind im https://docs.qgis.org/3.28/de/docs/user_manual/working_with_vector/vector_properties.html#temporal-properties[QGIS User Manual] zu finden.

Anschliessend muss das "`Zeitsteuerungsfenster`" geöffnet werden (1). Darin kann dann die Funktionalität auch gestartet werden (2).

image::zeitreise_mit_gqis_temporale_daten_visualisieren_gifs_erstellen/img5.png[]

In dem gestarteten "`Zeitsteuerungsfenster`" kann dann die Zeitspanne gesetzt (1) und die zeitliche Schrittgrösse (2) definiert werden. Über die Steuerungsknöpfe (3) kann die zeitliche Abfolge dann gesteuert werden.

image::zeitreise_mit_gqis_temporale_daten_visualisieren_gifs_erstellen/img6.png[]

TIP: *Anmerkung*: Die festgelegte Zeitspanne zwischen Start und Ende definiert die Gültigkeit der angezeigten Datenpunkte, wodurch nur Informationen innerhalb dieses Zeitbereichs sichtbar sind, um zeitliche Veränderungen zu veranschaulichen und zu analysieren.


[#U1]
== Übung 1: Zeitsteuerung in QGIS

. Nun soll für den Layer "`Gebauede Zuerich`" die dynamische Zeitsteuerung aktiviert werden. Dafür soll die Konfiguration "Einzelfeld mit Datum/Zeit" gewählt werden. Als Feld muss "`baujahr`" gewählt werden. Zudem sollte das Kreuz "`Objekte über Zeit sammeln`" angewählt werden.

. Sobald im Layer die dynamische Zeitsteuerung angewählt ist, kann die Zeitsteuerung aktiviert werden. Findet anschliessend einen passenden Animationsbereich, um die Entwicklung der Stadt gut darzustellen.



== Theorie: Zusätzliche Informationen zur Visualisierung

Im Folgenden wird erläutert, wie die animierte Visualisierung mit Beschriftungen (Labels) und Dekorationen als zusätzliche Informationen angereichert werden kann.


=== Beschriftung (Label) einfügen

Damit eine Beschriftung (Label) hinzugefügt werden kann, sollte ein zusätzlicher Layer erstellt werden. Dieser kann im Tabellennamen z.B. mit Information benannt werden. Der neue Layer kann gleich im bestehenden QGIS-Projekt erstellt werden. Dazu wird ein neuer GeoPackage-Layer erzeugt und die bestehende GeoPackage-Datei als Datenbank gewählt.

image::zeitreise_mit_gqis_temporale_daten_visualisieren_gifs_erstellen/label1.png[]

Der Geometrietyp soll ein Punkt sein. Ein neues Feld dazu wird nicht benötigt. Als Koordinatensystem kann hier entsprechend der Datenherkunft "`EPSG:2056 - Swiss CH1903+ / LV95`" gewählt werden.

image::zeitreise_mit_gqis_temporale_daten_visualisieren_gifs_erstellen/label2.png[]

Um ein Label hinzuzufügen, kann nun ein Punkt auf dem Layer hinzugefügt werden. Dazu sollte der Layer zuerst in den Editiermodus gewechselt werden (1) und dann kann über die Punkt Funktion (2) ein Punkt auf der Karte hinzugefügt werden, da wo das Label erscheinen soll.

image::zeitreise_mit_gqis_temporale_daten_visualisieren_gifs_erstellen/label3.png[]

Um das Label zu definieren, muss die Layergestaltungsansicht geöffnet werden. Hier kann dann z.B. das Symbol für den hinzugefügten Punkt ausgeblendet werden (1) und ein einzelnes Symbol hinzugefügt werden (2).

image::zeitreise_mit_gqis_temporale_daten_visualisieren_gifs_erstellen/label4.png[]

image::zeitreise_mit_gqis_temporale_daten_visualisieren_gifs_erstellen/label5.png[]


=== Dekorationen

Weiter gibt es die Möglichkeit diverse "`Dekorationen`" der Karte hinzuzufügen. Dekorationen sind grafische Elemente wie z.B. Titel, Massstab oder Nordpfeil, die einer Karte hinzugefügt werden können. Dazu wird der Menüpunkt menu:Ansicht[Dekorationen] verwendet.

image::zeitreise_mit_gqis_temporale_daten_visualisieren_gifs_erstellen/deko1.png[]

Zusätzliche Informationen sind im https://docs.qgis.org/3.28/de/docs/user_manual/map_views/map_view.html#decorating-the-map[QGIS User Manual] zu finden.



== Übung 2: Ergänzende Informationen

Nach <<#U1>> solltet Ihr eine schöne Übersicht der Entwicklung der Stadt Zürich haben. Damit nun aber auch ersichtlich wird, um welches Jahr es sich jeweils handelt und die ganze Darstellung übersichtlicher wird, sollten noch folgende Schritte ergänzt werden:

. Erstellt einen neuen Layer (menu:Layer[Neuer Layer > GeoPackage Layer...])
Als Datenbank kann wieder das Gebaeude "`Zuerich.gpkg`" verwendet werden.
Der Geometrietyp ist "`Punkt`".
. Nun muss der Layer bearbeitet werden und ein Punkt an dem Ort hinzugefügt werden, wo das Label mit der Jahreszahl erscheinen soll.
. Geht zu den Layergestaltung und ändert ihn auf keine Symbole
. Wechselt bei der Layergestaltung auf die Beschriftung und wählt "`Einzelne Beschriftung`". Hier kann dann auch gleich das Label editiert werden.
. Für die Beschriftung kann nun eine Formel hinterlegt werden:
`format_date(@map_start_time, 'yyyy')`

Als Nächstes wollen wir noch einen Titel hinzufügen

. Fügt einen Titel hinzu (menu:Ansicht[Dekorationen > Titelbeschriftung...])
. Aktiviert die Titelbeschriftung und wählt einen Titel. (z.B. Zürich)
. Der Titel kann hier noch nach Belieben gestaltet werden.

Zum Schluss möchten wir noch eine Legende hinzufügen. Dazu gibt es im Aufgaben Zip bereits ein Legende.gif.

. Fügt das Legnde.gif Bild hinzu (menu:Ansicht[Dekorationen > Bild...])
. Die Platzierung und Grösse können nach Belieben angepasst werden.



== Theorie: Animierte GIF-Dateien

Die Zeitsteuerung bietet noch eine Exportfunktion. Damit können die jeweiligen Kartenausschnitte entsprechend der Schrittgrösse als eine Serie von Bildern abgespeichert werden. 

Aus diesen Einzelbildern kann nun ein so genanntes "`animiertes`" GIF (Graphics Interchange Format) erstellt werden. Ein animiertes GIF ist ein digitales Bildformat, das mehrere Bilder in einer einzigen Datei zusammenfasst, um eine kurze, sich wiederholende Animation (ohne Ton) zu erzeugen, die in einer Endlosschleife abgespielt wird. Normalerweise sind die Bilder alphabetisch nach Dateinamen sortiert. Animierte GIFs werden von allen gängigen Browsern unterstützt.

Für den Export der Bilder ist in QGIS der btn:[Symbol-Knopf] für Speichern zu verwenden.

image::zeitreise_mit_gqis_temporale_daten_visualisieren_gifs_erstellen/theorie1.png[]

Weitere Informationen zur Exportfunktion sind im https://docs.qgis.org/3.28/de/docs/user_manual/map_views/map_view.html#maptimecontrol[QGIS User Manual] zu finden.



== Übung 3: Animiertes GIF erstellen

Zum Schluss möchten wir nun ein animiertes GIF mit der entsprechenden Übersicht erstellen. 

Folgende Schritte sind dazu nötig.

. Die Zeitsteuerung bietet eine Exportfunktion an. Dazu kann auf den Knopf btn:[Speichern] geklickt werden.
. Es speichert die Bilder der einzelnen Zeitpunkte in einem DateiVerzeichnis ab.
. Diese Bilder können z.B. über die freie "https://ezgif.com/maker[Webapplikation "Animated GIF Maker" (ezgif)]" in ein einziges, animiertes GIF umgewandelt werden.

TIP: Bei animierten GIFs ist der Übergang vom letzten Bild zum ersten meist abrupt und man hat als Betrachter kaum Zeit, sich den Endzustand anzusehen. Hier gibt es einen Trick: Man kopiert das letzte Bild z.B. 5 mal und ändert die Dateinamen entsprechend (normalerweise alphabetisch aufsteigend). Jetzt bleibt der letzte Zustand viel länger stehen. 


== Abschluss und Ausblick

Die Erfahrung im Umgang mit zeitlichen Daten zeigt, dass die Genauigkeit der Zeit-Werte entscheidend für aussagekräftige Ergebnisse ist. 

Die temporale Datenhaltung in QGIS ermöglicht interessante Visualisierungen wie hier die Gebäudeentwicklung der Stadt Zürich. Die Kombination von raum-zeitlichen Daten erlaubt nicht nur die Visualisierung von Veränderungen im Zeitverlauf, sondern auch weitergehende Analysen wie das Erkennen von Mustern oder Trends. 

Es gibt eine Vielzahl weiterer Möglichkeiten der raum-zeitlichen Analyse, wie z.B. die Visualisierung einer Reise, wenn eine mobile GPS-Tracking-Applikation mitgeführt wird. Wieder zurück können die GPX-Trackdaten ausgewertet werden. Die zeitliche Komponente ist ein wichtiger Aspekt in der Geodatenanalyse, der in verschiedenen Bereichen wie Forschung, Stadtplanung und Umweltstudien vielfältige Anwendung findet.

TIP: **Hinweis**: Vorsicht bei Shapefiles als Eingabedaten! Bei deren Verwendung als Eingabedaten ist zu beachten, dass es sich um ein veraltetes Dateiformat handelt; wenn möglich sollte z.B. GeoPackage verwendet werden. Ein Problem von Shapefiles ist, dass sie für die Zeit nur den Datentyp "`Date`" (Tagesdatum) kennen, aber keine Zeitangabe. Falls eine Zeitangabe erforderlich ist, wird empfohlen, https://de.wikipedia.org/wiki/ISO_8601[ISO-8601] zu verwenden: z.B. "`2023-12-24T18:21`" (ohne Angabe der Zeitzone), "`2023-12-24T18:21Z`" (für die universelle Zeitzone UTC) oder "`2023-12-24T18:21+01:00`" (für Zeitzone +2 Stunden, das auch für die Mitteleuropäische Sommerzeit nötig ist). 

== Weiterführende Quellen

Dieses Unterrichtsmaterial ist Teil von https://www.openschoolmaps.ch/[OpenSchoolMaps], insbesondere des Themas "`Einführung in QGIS 3 und in Geoinformationssysteme (GIS)`". Wer mehr Informationen sucht, sollte dort zuerst nachschauen.

.Das OpenSchoolMaps-Logo.
image::logo_openschoolmaps_web_128x87.png[]


Weiterführende Quellen sind:

* https://de.wikipedia.org/wiki/Temporale_Datenhaltung
* https://de.wikipedia.org/wiki/Geodaten
* https://www.qgistutorials.com/de/docs/3/animating_time_series.html


== Wissenstest

Frage 1: Was bewirkt die Checkbox "Objekte über Zeit sammeln"?

ifdef::show_solutions[]
====
.Antwort: 1
Die erstellte Karte sieht wie folgt aus: Es führt dazu, dass die Punkte
nach dem Erscheinungszeitpunkt auch weiterhin dargestellt werden.
====
endif::show_solutions[]

Frage 2: Was macht es in diesem Anwendungsfall etwas schwieriger?

ifdef::show_solutions[]
====
.Antwort: 2
Da die Stadt in den ersten Hunderten von Jahren viel langsamer gewachsen
ist, ist es schwierig, ein passendes Intervall zu finden.
====
endif::show_solutions[]

Frage 3: Wie könnte das obige Problem gelöst werden?

ifdef::show_solutions[]
====
.Antwort: 3
Man könnte z.B. für die Jahre bis 1750 einen eigenen Layer erstellen,
welcher ein Intervall von 50 Jahren enthält und dann ab 1950 einen
weiteren, welcher ein Intervall von 10 Jahren verwendet.
====
endif::show_solutions[]


== ANHANG: Datenbeschreibung

[horizontal]
Dateiname:: KTZH_00002022_00004064.csv 
Beschreibung:: Gebäude Zürich vom Jahr 1200 bis 2020. Vollständig, gute logische Konsistenz. Die Daten wurden von https://opendata.swiss/de/dataset/gebaude-und-wohnungen-im-kanton-zurich[opendata.swiss] bezogen und dann für die Stadt Zürich gefiltert. 
Format::                    CSV             
Koordinatenreferenzsystem:: EPSG:2056 - Swiss CH1903+ / LV95 
Geometrie::                 Point           
Genauigkeit::               Dezimeter bis Zentimeter 
Aktualität::                2. November 2023, Aktualisierungsintervall vierteljährlich 
Lizenz/Nutzungsrechte::     CC0: Freie Nutzung. Sie dürfen diesen Datensatz auch für kommerzielle Zwecke nutzen. Eine Quellenangabe ist Pflicht (Autor, Titel und Link zum Datensatz). 

image:license.png[] Frei verwendbar unter der Lizenz (c) http://creativecommons.org/publicdomain/zero/1.0/[CC0 1.0], sofern nicht anders angegeben.


